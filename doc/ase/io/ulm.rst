ULM files
=========

.. automodule:: ase.io.ulm
    :members: ulmopen

.. autoclass:: Writer
    :members:

.. autoclass:: Reader
    :members:

.. autoexception:: InvalidULMFileError
